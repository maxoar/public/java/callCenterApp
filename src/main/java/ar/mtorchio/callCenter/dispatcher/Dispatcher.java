package ar.mtorchio.callCenter.dispatcher;

import ar.mtorchio.callCenter.bo.EmployeeBO;
import ar.mtorchio.callCenter.concurrent.HoldedCall;
import ar.mtorchio.callCenter.concurrent.OngoingCall;
import ar.mtorchio.callCenter.domain.call.Call;
import ar.mtorchio.callCenter.domain.employee.Employee;
import ar.mtorchio.callCenter.service.CallsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ConcurrentLinkedQueue;

@Component
public class Dispatcher implements Observer {

    @Autowired
    CallsService callsServiceImpl;

    @Autowired
    EmployeeBO employeeBO;

    @Autowired
    private TaskExecutor callsTaskExecutor;

    private ConcurrentLinkedQueue<Call> holdedCalls;

    @PostConstruct
    public void init(){
        this.holdedCalls = new ConcurrentLinkedQueue<>();
    }

    public Call dispatchCall(Call incomingCall) {
        incomingCall = callsServiceImpl.createCall(incomingCall);
        Employee availableEmployee = getAvailableEmployee();
        if ( Objects.nonNull( availableEmployee ) ) {
            availableEmployee.addObserver(this);
            callsTaskExecutor.execute( new OngoingCall(incomingCall, availableEmployee));
        } else {
            holdedCalls.add(incomingCall);
            callsTaskExecutor.execute( new HoldedCall(incomingCall));
        }
        return incomingCall;
    }

    private Employee getAvailableEmployee(){
        Employee availableEmployee = employeeBO.getAvailableOperator();
        if( Objects.isNull( availableEmployee ) ) {
            availableEmployee = employeeBO.getAvailableSupervisor();
        } else if( Objects.isNull( availableEmployee ) ) {
            availableEmployee = employeeBO.getAvailableDirector();
        }
        return availableEmployee;
    }

    @Override
    public void update(Observable o, Object arg) {
        answerHoldedCalls(o, arg);
    }

    private void answerHoldedCalls(Observable o, Object arg){
        if( ! holdedCalls.isEmpty() ){
            Call holdedCall = holdedCalls.poll();
            Employee employee = (Employee) o;
            if( employee.isAvailable() ){
                holdedCall.unHold();
                callsTaskExecutor.execute( new OngoingCall(holdedCall, employee));
            }
        }
    }
}