package ar.mtorchio.callCenter.domain.employee;

import lombok.*;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Data
public class Director extends Employee {

    public Director(String userName){
        super(userName);
    }

}


