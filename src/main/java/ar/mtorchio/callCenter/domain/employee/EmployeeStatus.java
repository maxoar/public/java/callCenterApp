package ar.mtorchio.callCenter.domain.employee;

public enum EmployeeStatus {

    Available,
    PendingCall,
    InCall,
    NotAvailable
}
