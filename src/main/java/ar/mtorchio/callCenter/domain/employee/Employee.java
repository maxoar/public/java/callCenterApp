package ar.mtorchio.callCenter.domain.employee;

import ar.mtorchio.callCenter.domain.call.Call;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

@Log4j2
@Data
public abstract class Employee extends Observable implements Serializable, Observer {

    private static final long serialVersionUID = 1L;

    //@Id
    private String id;

    private EmployeeStatus status;

    @ToString.Exclude
    private Call call; //Observable

    @NonNull
    //@Indexed(unique = true)
    private String userName;

    public Boolean isAvailable(){
        return this.status == EmployeeStatus.Available;
    }

    public void update(Observable o, Object arg) {
        this.call = (Call) o;
        if ( this.call.isOngoing() ) {
            this.status = EmployeeStatus.InCall;
            log.info(">>> In call: " + this.getUserName() );
        } else if ( this.call.isFinished() ) {
            this.status = EmployeeStatus.Available;
            log.info(">>> Available: " + this.getUserName());
        }
        setChanged();
        notifyObservers();
    }

}
