package ar.mtorchio.callCenter.domain.employee;

import lombok.Data;
import lombok.extern.log4j.Log4j2;

@Data
@Log4j2
public class Supervisor extends Employee {

    public Supervisor(String userName){
        super(userName);
    }
}