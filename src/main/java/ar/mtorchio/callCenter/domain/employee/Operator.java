package ar.mtorchio.callCenter.domain.employee;


import lombok.Data;
import lombok.extern.log4j.Log4j2;


@Data
@Log4j2
public class Operator extends Employee {

    public Operator(String userName){
        super(userName);
    }
}
