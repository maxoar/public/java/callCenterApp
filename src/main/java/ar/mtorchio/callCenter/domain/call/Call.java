package ar.mtorchio.callCenter.domain.call;

import ar.mtorchio.callCenter.domain.employee.Employee;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.Observable;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Call extends Observable implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    @NonNull
    private Phone phoneNumber;

    @NonNull
    private Date incomingTime;

    private CallStatus status;

    @ToString.Exclude
    private Employee attendingCallEmployee;

    private Date startTime;
    private Date endTime;

    public void hold(){
        this.status = CallStatus.Holded;
        setChanged();
        notifyObservers( CallStatus.Holded );
    }

    public void unHold(){
        this.status = CallStatus.Unhold;
        setChanged();
        notifyObservers( CallStatus.Unhold );
    }

    public void start(){
        addObserver(attendingCallEmployee);
        this.status = CallStatus.Ongoing;
        this.startTime = new Date();
        setChanged();
        notifyObservers( CallStatus.Ongoing );
    }

    public void end() {
        this.status = CallStatus.Finished;
        this.endTime = new Date();
        setChanged();
        notifyObservers( CallStatus.Finished );
        deleteObservers();
    }

    public Boolean isOngoing(){ return this.status == CallStatus.Ongoing; }
    public Boolean isHolded(){ return this.status == CallStatus.Holded; }
    public Boolean isFinished(){ return this.status == CallStatus.Finished; }

}

