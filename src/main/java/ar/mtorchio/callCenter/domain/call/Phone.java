package ar.mtorchio.callCenter.domain.call;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Phone implements Serializable {

    private String countryCode;

    private String areaCode;

    @NonNull
    private String number;
}
