package ar.mtorchio.callCenter.domain.call;

public enum CallStatus {

    Incoming,
    Ongoing,
    Holded,
    Unhold,
    Finished,
    Missed
}
