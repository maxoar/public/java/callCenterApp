package ar.mtorchio.callCenter.domain.errors;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RestError {

    private HttpStatus httpStatus;
    private String message;
    private String debugMessage;

}
