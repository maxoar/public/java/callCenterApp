package ar.mtorchio.callCenter.rest.controller;

import ar.mtorchio.callCenter.dispatcher.Dispatcher;
import ar.mtorchio.callCenter.domain.call.Call;
import ar.mtorchio.callCenter.dto.CallDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class DispatcherController {

    @Autowired
    Dispatcher dispatcher;

    @PostMapping(value="/dispatcher/dispatch/call/", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> dispatchCall(@RequestBody CallDTO callDTO) {
        callDTO.validateCallDTO();
        Call incomingCall = dispatcher.dispatchCall( callDTO.toCall() );
        return ResponseEntity.ok().body( new CallDTO(incomingCall) );
    }

}
