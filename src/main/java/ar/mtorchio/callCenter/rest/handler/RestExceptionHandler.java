package ar.mtorchio.callCenter.rest.handler;

import ar.mtorchio.callCenter.domain.errors.RestError;
import ar.mtorchio.callCenter.rest.exceptions.InvalidCallException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler{// extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<RestError> handleException(Throwable ex) {
        String error = "Internal Server Error";
        return buildResponseEntity( new RestError( HttpStatus.INTERNAL_SERVER_ERROR, error, ex.getLocalizedMessage() ) );
    }

    @ExceptionHandler(InvalidCallException.class)
    public ResponseEntity<RestError> exceptionInvalidOrderException(InvalidCallException ex) {
        String error = "Malformed JSON request";
        return buildResponseEntity(new RestError(HttpStatus.BAD_REQUEST, error, ex.toString()));
    }

    public ResponseEntity<RestError> buildResponseEntity(RestError apiError) {
        return new ResponseEntity<>(apiError, apiError.getHttpStatus());
    }
}
