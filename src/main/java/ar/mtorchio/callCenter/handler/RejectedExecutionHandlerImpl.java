package ar.mtorchio.callCenter.handler;

import lombok.extern.log4j.Log4j2;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

@Log4j2
public class RejectedExecutionHandlerImpl implements RejectedExecutionHandler {

    private static final String NAME = "OngoingCall - Executor";
    private static final Long SLEEP_TIME_MILISECONDS = 1000L;

    @Override
    public void rejectedExecution(Runnable runnable, ThreadPoolExecutor executor) {
        log.info( "----->| LOCK THE ENTRY TO THREAD POOL EXECUTOR - [" + NAME + "] "
                + "- Waiting time to re-execute: " + SLEEP_TIME_MILISECONDS
                + " miliseconds. ThreadPoolSize: " + executor.getPoolSize() );
        try {
            Thread.sleep(SLEEP_TIME_MILISECONDS);
        } catch (InterruptedException ie) {
            log.error(ie);
        }
        executor.execute(runnable);
    }

}
