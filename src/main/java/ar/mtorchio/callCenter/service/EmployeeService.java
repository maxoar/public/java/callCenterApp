package ar.mtorchio.callCenter.service;

import ar.mtorchio.callCenter.domain.employee.Employee;

import java.util.List;

public interface EmployeeService {

    List<Employee> getOnlineEmployees();

}
