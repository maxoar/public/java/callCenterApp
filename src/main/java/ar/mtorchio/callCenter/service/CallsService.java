package ar.mtorchio.callCenter.service;

import ar.mtorchio.callCenter.domain.call.Call;
import org.springframework.stereotype.Service;


@Service
public interface CallsService {

    Call createCall(Call newCall);

}
