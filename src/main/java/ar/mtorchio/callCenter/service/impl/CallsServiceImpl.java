package ar.mtorchio.callCenter.service.impl;

import ar.mtorchio.callCenter.domain.call.Call;
import ar.mtorchio.callCenter.domain.employee.Employee;
import ar.mtorchio.callCenter.service.CallsService;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Log4j2
@Component
public class CallsServiceImpl implements CallsService {

    //Calls service URL.
    private static final String CALLS_SERVICE = "";

    @Autowired
    RestTemplate restTemplate;

    @Override
    public Call createCall(Call newCall){
        return restTemplate.getForEntity(CALLS_SERVICE, Call.class).getBody();
    }

}
