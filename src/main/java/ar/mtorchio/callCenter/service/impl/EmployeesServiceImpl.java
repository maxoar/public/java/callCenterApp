package ar.mtorchio.callCenter.service.impl;

import ar.mtorchio.callCenter.domain.employee.Employee;
import ar.mtorchio.callCenter.domain.employee.Operator;
import ar.mtorchio.callCenter.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeesServiceImpl implements EmployeeService {

    //Employees service URL.
    private static final String EMPLOYEES_SERVICE = "";

    @Autowired
    RestTemplate restTemplate;

    @Override
    public List<Employee> getOnlineEmployees(){
        return restTemplate.getForEntity(EMPLOYEES_SERVICE, List.class).getBody();
    }
}
