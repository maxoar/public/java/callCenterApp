package ar.mtorchio.callCenter.concurrent;

import ar.mtorchio.callCenter.domain.call.Call;
import ar.mtorchio.callCenter.domain.employee.Employee;
import ar.mtorchio.callCenter.domain.employee.EmployeeStatus;
import lombok.extern.log4j.Log4j2;

import java.util.Random;

@Log4j2
public class OngoingCall implements Runnable {

    public static final int MIN_SECOND_DURATION = 5;
    public static final int MAX_SECOND_DURATION = 10;

    private Call call;

    public OngoingCall(Call call, Employee employee){
        this.call = call;
        this.call.setAttendingCallEmployee(employee);
        this.call.addObserver(employee);
    }

    @Override
    public void run() {
        log.info("Starting call: " + call.getPhoneNumber());
        log.info("Answered by: " + call.getAttendingCallEmployee().getUserName() );
        call.start();
        try {
            Thread.sleep( getRandomNumberInRange(MIN_SECOND_DURATION, MAX_SECOND_DURATION)*1000 );
        } catch (InterruptedException e) {
            log.error(e);
            e.printStackTrace();
        }
        log.info("End call: " + call);
        call.end();
    }

    private int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min.");
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}
