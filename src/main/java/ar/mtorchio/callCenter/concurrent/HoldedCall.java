package ar.mtorchio.callCenter.concurrent;

import ar.mtorchio.callCenter.domain.call.Call;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class HoldedCall implements Runnable {

    public static final long SLEEP_TIME_MILISECONDS = 3000L;

    private Call call;

    public HoldedCall(Call call){
        this.call = call;
        call.hold();
    }

    @Override
    public void run() {
        while( call.isHolded()  ){
            log.info("Holded call :: " + call.getPhoneNumber() );
            try {
                Thread.sleep( SLEEP_TIME_MILISECONDS );
            } catch (InterruptedException e) {
                log.error(e);
                e.printStackTrace();
            }
        }

    }
}

