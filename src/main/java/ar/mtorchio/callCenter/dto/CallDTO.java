package ar.mtorchio.callCenter.dto;

import ar.mtorchio.callCenter.domain.call.Call;
import ar.mtorchio.callCenter.rest.exceptions.InvalidCallException;
import lombok.*;

import java.util.Date;
import java.util.Objects;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class CallDTO {

    private String id;

    @NonNull
    private PhoneDTO phoneNumber;

    @NonNull
    private Date incomingTime;

    public CallDTO(Call source){
        this.setId( Objects.nonNull( source.getId() ) ? source.getId().toString() : null );
        this.setPhoneNumber( new PhoneDTO(source.getPhoneNumber()) );
        this.setIncomingTime( source.getIncomingTime() );
    }

    public Call toCall(){
        return new Call(
                this.phoneNumber.toPhone(),
                this.incomingTime
        );
    }

    public void validateCallDTO(){
        this.phoneNumber.validatePhoneDTO();
        if( Objects.isNull(incomingTime) ){
            throw new InvalidCallException("incomingTime", true, "Incoming time, must be supplied.");
        }
    }

}
