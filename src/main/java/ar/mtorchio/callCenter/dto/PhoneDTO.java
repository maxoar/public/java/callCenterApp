package ar.mtorchio.callCenter.dto;

import ar.mtorchio.callCenter.domain.call.Phone;
import ar.mtorchio.callCenter.rest.exceptions.InvalidCallException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PhoneDTO {

    private String countryCode;

    private String areaCode;

    @NonNull
    private String number;

    public PhoneDTO(Phone source){
        this.setCountryCode( source.getCountryCode() );
        this.setAreaCode( source.getAreaCode() );
        this.setNumber( source.getNumber() );
    }

    public Phone toPhone(){
        return new Phone(
                this.countryCode,
                this.areaCode,
                this.number
        );
    }

    public void validatePhoneDTO(){
        if( Objects.isNull(number) ){
            throw new InvalidCallException(number, true, "Number, must be supplied.");
        } else if (number.isEmpty()) {
            throw new InvalidCallException(number, true, "Number can't be empty.");
        } else if( ! number.matches("^\\d+") ) { //check if number;
            throw new InvalidCallException(number, true, "Number must be valid number.");
        }
    }
}
