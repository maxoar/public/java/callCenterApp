package ar.mtorchio.callCenter.bo;

import ar.mtorchio.callCenter.domain.employee.Director;
import ar.mtorchio.callCenter.domain.employee.Operator;
import ar.mtorchio.callCenter.domain.employee.Supervisor;
import ar.mtorchio.callCenter.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeBO {

    @Autowired
    EmployeeService employeeService;

    public Operator getAvailableOperator(){
        return (Operator) employeeService.getOnlineEmployees().stream()
                .filter( emp -> emp instanceof Operator && emp.isAvailable() )
                .findAny()
                .orElse(null);
    }

    public Supervisor getAvailableSupervisor(){
        return (Supervisor) employeeService.getOnlineEmployees().stream()
                .filter( emp -> emp instanceof Supervisor && emp.isAvailable() )
                .findAny()
                .orElse(null);
    }

    public Director getAvailableDirector(){
        return (Director) employeeService.getOnlineEmployees().stream()
                .filter( emp -> emp instanceof Director && emp.isAvailable() )
                .findAny()
                .orElse(null);

    }
}
