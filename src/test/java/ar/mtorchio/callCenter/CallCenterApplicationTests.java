package ar.mtorchio.callCenter;

import ar.mtorchio.callCenter.domain.call.Call;
import ar.mtorchio.callCenter.domain.employee.*;
import ar.mtorchio.callCenter.dto.CallDTO;
import ar.mtorchio.callCenter.dto.PhoneDTO;
import ar.mtorchio.callCenter.rest.controller.DispatcherController;
import ar.mtorchio.callCenter.service.CallsService;
import ar.mtorchio.callCenter.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=CallCenterApplication.class)
public class CallCenterApplicationTests {

    private static final String HOST = "http://localhost:8380";
    private static final String CALLS_DISPATCHER_API = "/api/dispatcher/dispatch/call/";

    private MockMvc mockMvc;

    @Autowired
    DispatcherController dispatcherController;

    @MockBean
    EmployeeService employeeService;

    @MockBean
    CallsService callsService;

    private List<Employee> employees;

    @Before
    public void setup() throws Exception {
        this.mockMvc = standaloneSetup(this.dispatcherController).build();

        Director director1 = new Director("DIR1");
        director1.setStatus(EmployeeStatus.Available);

        Supervisor supervisor1 = new Supervisor("SUPS1");
        supervisor1.setStatus(EmployeeStatus.Available);
        Supervisor supervisor2 = new Supervisor("SUPS2");
        supervisor2.setStatus(EmployeeStatus.Available);

        Operator operator1 = new Operator("OP1");
        operator1.setStatus(EmployeeStatus.Available);
        Operator operator2 = new Operator("OP2");
        operator2.setStatus(EmployeeStatus.Available);
        Operator operator3 = new Operator("OP3");
        operator3.setStatus(EmployeeStatus.Available);
        Operator operator4 = new Operator("OP4");
        operator4.setStatus(EmployeeStatus.Available);
        Operator operator5 = new Operator("OP5");
        operator5.setStatus(EmployeeStatus.Available);

        employees = new ArrayList<>();

        employees.add( director1 );
        employees.add( supervisor1 );
        employees.add( supervisor2 );
        employees.add( operator1 );
        employees.add( operator2 );
        employees.add( operator3 );
        employees.add( operator4 );
        employees.add( operator5 );
    }


    @Test
    public void dispatchCallApi_concurrent_Ok() throws Exception {

        // Mocking employees service
        when(employeeService.getOnlineEmployees()).thenReturn(employees);

        // Mocking calls service
        long mockCallId = 12345L;
        when(callsService.createCall(any(Call.class)))
                .thenAnswer(invocationOnMock -> {
                    ReflectionTestUtils.setField((Call) invocationOnMock.getArgument(0), "id", mockCallId);
                    return invocationOnMock.getArgument(0);
                }
        );

        PhoneDTO phoneDTO = new PhoneDTO("+54","011","");
        CallDTO callDTO = new CallDTO(phoneDTO, new Date());

        for(int i=0; i<10; i++){
            phoneDTO.setNumber( "0000000"+i );
            callApi(callDTO);
        }

        Boolean finished = false;
        while ( !finished ){
            synchronized (employees){
                finished = employees.stream().filter( emp -> ! emp.isAvailable() ).count() == 0;
            }
        }
    }

    private void callApi(CallDTO callDTO) throws Exception {
        this.mockMvc.perform( MockMvcRequestBuilders
                .post(HOST+CALLS_DISPATCHER_API)
                .content( asJsonString(callDTO) )
                .contentType( MediaType.APPLICATION_JSON) )
                .andExpect( status().isOk() )
                .andExpect( jsonPath("$.id", is("12345")) )
                .andExpect( jsonPath("$.phoneNumber.countryCode", is("+54")) )
                .andExpect( jsonPath("$.phoneNumber.areaCode", is("011")) )
                .andExpect( jsonPath("$.phoneNumber.number", is( callDTO.getPhoneNumber().getNumber() )) )
                .andExpect( jsonPath("$.incomingTime", is( callDTO.getIncomingTime().getTime() )));
    }


    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
