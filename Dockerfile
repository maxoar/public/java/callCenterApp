FROM maven:3-jdk-8 AS build
COPY . /project
WORKDIR /project
RUN mvn compile -e
