# Call Center Application

Calls dispatcher application.

## Objetivo
Representar el funcionamiento de un "call center" a través de una aplicación, con la funcionalidad de atender llamadas.


## Requerimiento
Existe un call center donde hay 3 tipos de empleados: operador, supervisor y director. El proceso de la atención de una llamada telefónica en primera
instancia debe ser atendida por un operador, si no hay ninguno libre debe ser atendida por un supervisor, y de no haber tampoco supervisores libres debe ser atendida por un director.


### Requerimientos técnicos
* Debe existir una clase Dispatcher encargada de manejar las llamadas, y debe contener el método dispatchCall para que las asigne a los empleados disponibles.
* El método dispatchCall puede invocarse por varios hilos al mismo tiempo.
* La clase Dispatcher debe tener la capacidad de poder procesar 10 llamadas al mismo tiempo (de modo concurrente).
* Cada llamada puede durar un tiempo aleatorio entre 5 y 10 segundos.
* Debe tener un test unitario donde lleguen 10 llamadas.


### Consideraciones no descriptas
* Cuando no existe un empleado disponible (operador, supervisor o director) la llamada se pone en espera, de ésta forma se podrían realizar ventas sobre distintos productos de la compañía, mientras el solicitante espera ser atendido.

* En caso de estar atendiendo 10 llamas simultáneas, la nueva solicitud se frena hasta que la misma pueda ser atendida por un operador disponible. En este caso, al igual que el anterior, podría existir un proceso de venta o musicalización de espera. Del mismo modo, podría rechazarse la llamada, solicitando que se vuelva a llamar en otro momento.


## Compilar
```
mvn compile -e
```

## Testar
```
mvn test -e
```

## Ejecutar
#### Consideraciones:
La api no es una solución completa en sí misma, por lo que se deberán implementar los servicios de "Empleados" y de "Llamadas".
```
java -jar target\callsCenter-api.jar
```